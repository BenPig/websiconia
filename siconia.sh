#!/bin/bash

#  siconia.sh
#  
#  Copyright 2023 Benoit Pigeon <websiconia@phyllum.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
# shellcheck disable=SC2317
#Usage "configuration" siconia.sh --configure
#Usage "capture" siconia.sh --capture

#Détermination du répertoire d'installation
DIR=$(pwd);
echo "Répertoire courant $DIR";
if [ -f "$DIR"/www-data/index.php ]; then
	echo "Répertoire d'installation $DIR";
elif [ -f "$DIR"/index.php ]; then
	DIR="$DIR"/..;
	echo "Répertoire d'installation $DIR";
else
	echo 'Répertoire indéterminable';
	exit 10;
fi;
#Chargement des paramètres utilisateur
source "$DIR"/siconia.cfg;
#Fichier de capture
TTY_FILE="$DIR"/siconia.tty;
#Fichier de log
LOG_FILE="$DIR"/siconia.log;

#Configuration
function configure(){
	_set_tmp_files;
	_perms;
	_capture;
	_test;
	return $?;
}

function _set_tmp_files(){
	#Set/reset tty file
	touch "$TTY_FILE";
	cat /dev/null > "$TTY_FILE";
	#Set/reset log file
	touch "$LOG_FILE";
	cat /dev/null > "$LOG_FILE";
}
#Octroi des permissions
function _perms(){
	echo "Répertoire d'installation : $DIR";
	#Groupe prop for all
	chgrp -R "$WEB_GRP" "$DIR";
	#Perms glogal
	#Nothing for others
	chmod -R o-rwX "$DIR"/*;
	#Read for all
	chmod g+rX "$DIR"/*;
	chmod g-w "$DIR"/*;
	#Write for tty and log
	chmod g+rw "$TTY_FILE";
	chmod g+rw "$LOG_FILE";
	#READ for PORT
	chown "$WEB_USR" "$PORT";
	chmod u+r "$PORT";
}
#Test de la capture
function _test(){
	#Code Temps
	SIGNATURE='0-0:1.0.0(';
	if grep -R "$SIGNATURE" "$TTY_FILE";
	then
		echo 'Test réussi';
		return 0;
	else
		echo 'Test échoué';
		return 2;
	fi
}
function capture(){
	#Fais 3 tests
	_capture;
	TT="$?";
	echo 'Retour de _capture='$TT;
	_test;
	TT="$?";
	echo 'Retour de _test (1)='$TT;
	if [ $TT == 0 ]; then
		return 0;
	else
		sleep 3;
		_capture;
		_test;
		TT="$?";
		echo 'Retour de _test (2)='$TT;
		if [ $TT == 0 ]; then
			return 0;
		else
			sleep 3;
			_capture;
			_test;
			TT="$?";
			echo 'Retour de _test (3)='$TT;
			if [ $TT == 0 ]; then
				return 0;
			else
				return 1;
			fi;
		fi;
	fi;
}

#Capture du flux sur le port série
function _capture(){
	#Reset du fichier
	touch "$TTY_FILE";
	cat /dev/null > "$TTY_FILE";
	#echo "$FILELOG reset";
	#echo $!;
	#Alternatives
	#CMD="cat $PORT > $TTY_FILE";
	#CMD="sudo screen /dev/ttyUSB0 115200";
	#CMD="sudo cu -l /dev/ttyUSB0 -s 115200";
	#CMD="/usr/bin/minicom --capturefile=$FILELOG --baudrate 115200 --device $PORT";
	#Configuration du port
	echo PORT="$PORT" SPEED="$SPEED" TTY_FILE="$TTY_FILE"
	setserial "$PORT" baud_base "$SPEED";
	#Capture asynchrone
#	screen  "$PORT" 115200 > "$TTY_FILE" &
#	minicom --capturefile="$TTY_FILE" --baudrate 115200 --device "$PORT" &
	cat "$PORT" > "$TTY_FILE" &
#	cu -l "$PORT" -s 115200 > "$TTY_FILE" & 
	PID=$!;
	echo "$PID";
	#Capture durant 2 seconde
	sleep 2;
	#Arrêt du processus de capture
	kill -s SIGTERM "$PID";
#	echo "$PID"' tué';
	echo 'Capture effectuée';
	return 0;
}

if [ "$1" == '--configure' ]; then
	configure;
	RT=$?;
	echo "Retour de configure $RT";
	exit $RT;
elif [ "$1" == '--capture' ]; then
	capture;
	RT=$?;
	echo "Retour de capture $RT";
	exit $RT;
else
	echo "Action non-reconnue $1";
	exit 1;
fi;
