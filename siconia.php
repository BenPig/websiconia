
<?php

/*
 * siconia.php
 * 
 * Copyright 2023 Benoit Pigeon <websiconia@phyllum.be>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
class Num2String{
	static function add($nr1, $nr2){
		if (is_numeric($nr1) and is_numeric($nr2)) return $nr1+$nr2;
		else return $nr1.'+'.$nr2;
	}
	
	static function times($nr1, $nr2){
		if (is_numeric($nr1) and is_numeric($nr2)) return $nr1*$nr2;
		else return $nr1.'*'.$nr2;
	}
	static function div($nr1, $nr2){
		if (is_numeric($nr1) and is_numeric($nr2)) return $nr1/$nr2;
		else return $nr1.'/'.$nr2;
	}
	static function intval($nr1){
		if (is_numeric($nr1)) return intval($nr1);
		else return $nr1;
	}
	static function floatval($nr1){
		if (is_numeric($nr1)) return floatval($nr1);
		else return $nr1;
	}
}
class Sys{
	private $log=' ';
	private $showLog;
	private $storeLog=true;
	private $FORMAT;
	private $LOGFile='/srv/httpd//webSiconia/siconia.log';
	private $handle;
	public $content;
	private $ErrPage;
	#Initialisation
	function __construct($format, $showLog, $ErrPage){
		$this->LOGFile='/srv/httpd//webSiconia/siconia.log';
		$this->handle=fopen($this->LOGFile,'w');
		$this->showLog=$showLog;
		$this->FORMAT=$format;
		$this->ErrPage=$ErrPage;
		$this->log("Sys->constructor($format, $showLog, $ErrPage)", 'BEGIN');
	}
	
	function appendLine($text){
		//trace('Write in '.$this->pathfilename.' '.$text);
		//fwrite($this->handle,utf8_encode($text)."\n");
		fwrite($this->handle,$text."\n");
	}
	
	#Enregistrement d'un log
	function log($context,$msg){
		$date = date('d/m/Y H:i:s');
		$this->appendLine('INFO=>'.$context.':'.$msg);
		//if ($this->storeLog) error_log('INFO=>'.$context.':'.$msg);
		switch($this->FORMAT) {
			case 'HTML':
				$this->log=$this->log.'<p>' .$date.':'.$context.' => '.$msg.'</p>';
				break;
			case 'JSON':
				$this->log=$this->log.'{" '.$date.':'.$context.' => '.$msg.'|||"},'."\n";
				break;
		}
	}
	#Déclenchement d'une erreur
	function err($context,$msg){
		$this->appendLine('ERREUR=>'.$context.':'.$msg, $this->LOGFile);
		$this->content=$this->loadFile($this->ErrPage);
		$this->content = str_replace('#ERR#', $msg, $this->content);
		$this->send();
		die;
	}
	#Envoi de la page
	function send(){
		if ($this->showLog) {
			# suppression de la dernière "," + fermeture/ajout "}"
			if ($this->FORMAT == 'JSON')  $this->msg_log=substr($this->log,0,strlen($this->log)-1).']';
			#Dans les 2 cas
			$this->content = str_replace('#LOG#', $this->log, $this->content);
		}
		else $this->content = str_replace('#LOG#', ' ', $this->content);
		
		if (str_contains($this->content,'#ERR#')) $this->content = str_replace('#ERR#', ' ', $this->content);
		
		print $this->content;
	}
	#Chargement d'un fichier en string
	function loadFile($file){
		$this->log("sys->loadFile($file)","Opening $file");
		$content=file_get_contents($file);
		#$this->log("sys->loadFile($file)",'content='.$content);
		return $content;
	}
	//#ajout d'une ligne daans un fichier
	//function appendLine($line,$file){
		//file_put_contents($file, $line."\n", FILE_APPEND | LOCK_EX);
	//}
}

class Siconia{
	const MATCH_CODE=array(
			'TIME_SIC' => array('0-0:1.0.0','NULL'),
			'CURRENT_RATE' => array('0-0:96.14.0','NULL'),
			'IDX_PRELEV_TOT' => array('1-0:1.8.0','kWh'),
			'IDX_PRELEV_RATE1' => array('1-0:1.8.1','kWh'),
			'IDX_PRELEV_RATE2' => array('1-0:1.8.2','kWh'),
			'IDX_INJECT_TOT' => array('1-0:2.8.0','kWh'),
			'IDX_INJECT_RATE1' => array('1-0:2.8.1','kWh'),
			'IDX_INJECT_RATE2' => array('1-0:2.8.2','kWh'),
			'PW_PRELEV' => array('1-0:1.7.0','kW'),
			'PW_INJECT' => array('1-0:2.7.0','kW'),
			'VOLT_PH1' => array('1-0:32.7.0','V'),
			'AMP_PH1' => array('1-0:31.7.0','A'),
			'PW_PH1' => array('NULL','kVA'),
			'COSPHI_PH1' => array('NULL','%'),
			'PW_LIM' => array('0-0:17.0.0','kW'),
			'AMP_LIM' => array('1-0:31.4.0','A'));
	private $datas;
	private $sys;
	#Initialisation
	function __construct($sys){
		$this->sys=$sys;
		if (! $this->captureBash($sys)){
			$sys->err('Siconia->__construct()','Siconia error ');
		} else return true;
	}
	#capture via script Bash
	private function captureBash($sys){
#		$SICCMD='../captureSiconia.bash';
		$SICCMD='../siconia.sh --capture';
		$SICFILE='../siconia.tty';
#		$CMD=$SICCMD.' '.$SICFILE;
		$sys->log('captureBash()', $SICCMD);
		$ret;
		$output;
		#$ret=`$CMD`;
		#$ret=shell_exec($CMD);
		if (exec($SICCMD,$output,$ret)){
			$sys->log('Siconia->captureBash()', "ret=$ret");
			$sys->log('Siconia->captureBash()', print_r($output,true));
			if (filesize($SICFILE) > 0) {
				$this->datas=$sys->loadFile($SICFILE);
				#$this->sys->log('Siconia->getValue()','datas = |'.$this->datas.'|');
				return true;
			}
			else {
				$msg=print_r($output,true);
				$sys->err('Siconia->captureBash()','Siconia error :'.$msg);
				return false;
			}
		}
		else {
			$msg=print_r($output,true);
			$sys->err('Siconia->captureBash()','Siconia error :'.$msg);
			return false;
		}
	}
	//#Capture "direct"
	//private function capture($sys){
		//#Port de communication
		//$PORT='/dev/ttyUSB0';
		//#Vitesse (baud speed)
		//$SPEED=115200;
		//#Capture du flux sur le port pendant 1 seconde
		//$CMD='setserial '.$PORT.' baud_base '.$SPEED.'; cat '.$PORT.' > 1 & PID=$!; sleep 1 ; kill -s SIGKILL $PID ;';
		//$sys->log('capture()', $CMD);
		//exec($CMD,$rtn);
		//#$this->datas=`$CMD`;
		//$sys->log('capture()', print_r($rtn,true));
		//$this->datas=join("\n",$rtn);
		//$sys->log('capture()',$this->datas);
		//if ($this->datas) return true;
		//else return false;
	//}
	#Renvoi du code numérique du siconia
	function getNr(String $code){
		if (Siconia::MATCH_CODE[$code][0]) 
			if (Siconia::MATCH_CODE[$code][0] == 'NULL') return ' ';
			else return Siconia::MATCH_CODE[$code][0];
		else return 'NULL';
	}
	#Renvoi de l'unité attendue
	function getUnit(String $code){
		if (Siconia::MATCH_CODE[$code][1]) 
			if (Siconia::MATCH_CODE[$code][1] == 'NULL') return ' ';
			else return Siconia::MATCH_CODE[$code][1];
		else return 'NULL';
	}

	#Renvoi de la valeur
	function getValue(String $code){
		#$this->sys->log('Siconia->getValue()','code='.$code);
		switch($code) {
			case 'TIME_SIC':
				return $this->formatTime($this->extract($code));
			case 'IDX_PRELEV_TOT':
				$this->sys->log('Siconia->getValue()','value pleine='.$this->getValue('IDX_PRELEV_RATE1'));
				$this->sys->log('Siconia->getValue()','value creuse='.$this->getValue('IDX_PRELEV_RATE2'));
				return Num2String::add($this->getValue('IDX_PRELEV_RATE1'),$this->getValue('IDX_PRELEV_RATE2'));
			case 'IDX_INJECT_TOT':
				$this->sys->log('Siconia->getValue()','value pleine='.$this->getValue('IDX_INJECT_RATE1'));
				$this->sys->log('Siconia->getValue()','value creuse='.$this->getValue('IDX_INJECT_RATE2'));
				return Num2String::add($this->getValue('IDX_INJECT_RATE1'),$this->getValue('IDX_INJECT_RATE2'));
			case 'PW_PH1':
				return Num2String::div(Num2String::times($this->getValue('VOLT_PH1'),$this->getValue('AMP_PH1')),1000);
			case 'COSPHI_PH1':
				$pw=Num2String::add($this->getValue('PW_PRELEV'),$this->getValue('PW_INJECT'));
				$va=Num2String::div(Num2String::times($this->getValue('VOLT_PH1'),$this->getValue('AMP_PH1')),1000);
				$cosphi=Num2String::times(Num2String::div($pw,$va),100);
				return Num2String::intval($cosphi);
			default:
				return $this->extract($code);
		}
	}
	function extract($code){
		$nr=$this->getNr($code);
		if ($nr != 'NULL') {
			$unit=Siconia::MATCH_CODE[$code][1];
			$posCode=strrpos($this->datas,$nr);
			if ($posCode){
				#$this->sys->log('Siconia->getValue()','posCode = '.$posCode);
				$posDebut=strpos($this->datas,'(',$posCode);
				#$this->sys->log('Siconia->getValue()','postDebut = '.$posDebut);
				$posFin=strpos($this->datas,')',$posCode);
				#$this->sys->log('Siconia->getValue()','posFin = '.$posFin);
				$ret=substr($this->datas, $posDebut+1,($posFin-$posDebut-1));
				#$this->sys->log('Siconia->getValue()','ret = '.$ret);
				$value=explode('*',$ret);
				#$this->sys->log('Siconia->getValue()','value[0] = '.$value[0]);
				#$this->sys->log('Siconia->getValue()','value[1] = '.$value[1]);
				#si l'unité est correcte (unité attendue = unité dans le fichier)
				if ($value) {
					if (array_key_exists(1, $value) === true) {
						if ($unit == $value[1]) return Num2String::floatval($value[0]);
						else return 'INCOHERENT UNIT(1):'.$value[1].'/'.$unit;
					}
					elseif ($unit == 'NULL') return $value[0];
					else {
						$this->sys->log('Siconia->getValue()','datas = |'.$this->datas.'|');
						$this->sys->log('Siconia->getValue()','nr = |'.$nr.'|');
						$this->sys->log('Siconia->getValue()','ret = |'.$ret.'|');
						if (array_key_exists(0, $value)) $this->sys->log('Siconia->getValue()','value[0] = '.$value[0]);
						if (array_key_exists(1, $value)) $this->sys->log('Siconia->getValue()','value[1] = |'.$value[1].'|');
						$this->sys->log('Siconia->getValue()','unit = |'.$unit.'|');
						return 'INCOHERENT UNIT(2): /'.$unit;
					}
				}
				else {
					if ($unit == 'NULL') return $value;
					else return 'NO DATA';
				}
			}
			else return 'NOT_EXISTS';
		}
		else return 'NULL';
	}
	#Remplacement des tags par les valeurs
	function replace_in($code, $content){
		$content = str_replace('#'.$code.'_NR#', $this->getNr($code), $content);
		$content = str_replace('#'.$code.'_VALUE#', $this->getValue($code), $content);
		$content = str_replace('#'.$code.'_UNIT#', $this->getUnit($code), $content);
		return $content;
	}
	
	#Reformatage du temps format ISO-8601 déformé
	function formatTime($value){
		//le dernier caractère (le 12) doit être W (mais Siconia mais un S)
		$W=substr($value, 12,1); 
		if ($W =='W' or $W =='S' ) {
			$annee=substr($value, 0,2);
			$mois=substr($value, 2,2);
			$jour=substr($value, 4,2);
			$heure=substr($value, 6,2);
			$mins=substr($value, 8,2);
			$secs=substr($value, 10,2);
			return $jour.'/'.$mois.'/'.$annee.' '.$heure.':'.$mins.':'.$secs;
		}
		else return $value;
	}
}
#Fonction principale
function main($template,$errPage,$format, $showLog){
	$sys=new Sys($format, $showLog, $errPage);
	$sys->log('MAIN','begin');
	$siconia=new Siconia($sys);
	$sys->log('MAIN','Siconia done');
	$sys->content=$sys->loadFile($template);
	$sys->log('MAIN','Template loaded');
	$sys->content=$siconia->replace_in('TIME_SIC',$sys->content);
	$sys->content=$siconia->replace_in('CURRENT_RATE',$sys->content);
	$sys->content=$siconia->replace_in('IDX_PRELEV_TOT',$sys->content);
	$sys->content=$siconia->replace_in('IDX_PRELEV_RATE1',$sys->content);
	$sys->content=$siconia->replace_in('IDX_PRELEV_RATE2',$sys->content);
	$sys->content=$siconia->replace_in('IDX_INJECT_TOT',$sys->content);
	$sys->content=$siconia->replace_in('IDX_INJECT_RATE1',$sys->content);
	$sys->content=$siconia->replace_in('IDX_INJECT_RATE2',$sys->content);
	$sys->content=$siconia->replace_in('PW_PRELEV',$sys->content);
	$sys->content=$siconia->replace_in('PW_INJECT',$sys->content);
	$sys->content=$siconia->replace_in('VOLT_PH1',$sys->content);
	$sys->content=$siconia->replace_in('AMP_PH1',$sys->content);
	$sys->content=$siconia->replace_in('PW_PH1',$sys->content);
	$sys->content=$siconia->replace_in('COSPHI_PH1',$sys->content);
	$sys->content=$siconia->replace_in('PW_LIM',$sys->content);
	$sys->content=$siconia->replace_in('AMP_LIM',$sys->content);
	$sys->send();
}
?>
