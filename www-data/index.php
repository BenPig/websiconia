<?php

/*
 * Copyright 2023 Benoit Pigeon <websiconia@phyllum.be>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
require('../siconia.php');
$showLog=false;
$format='HTML';
$template;
$errPage;
if (isset($_GET['format'])) {
	$format=$_GET['format'];
}
if (isset($_GET['log'])) {
	$showLog=true;
}

switch($format) {
	case 'HTML':
		$template='../templates/siconia.html.model';
		$errPage='..//siconia.html.err';
		break;
	case 'JSON':
		$template='../templates/siconia.json.model';
		$errPage='../templates/siconia.json.model';
		break;
	default:
		sleep(5);
		print "Format inconnu ($format)";
}
main($template, $errPage,$format,$showLog);
?>
