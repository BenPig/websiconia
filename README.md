# WebSiconia

Un simple site Web pour visualiser (HTML) ou exporter (JSON) le contenu d'un compteur Siconia (SAGEMCOM S211).

Le système ne gére pas la sécurisation (authentification, accés, flux crypté, ...) (à faire gérer par le serveur Web).

# Pré-requis
Par défaut, l'ensemble des commandes ci-dessous seront éxecutées sous root ou précédée de sudo (dérivées d'Ubuntu ...) (noté avec [sudo], dans les commandes suivantes). De plus, l'utilisateur sera positionné dans le répertoire d'installation (nommé $WEBSICONIAD, ci-aprés).

- un compteur communicant Siconia (SAGEMCOM S211), dont le port P1 a été ouvert par le gestionnaire d'infrastructure (ORES, RESA, ...). (https://www.objetsconnectes.be/2021/02/26/jeedom-sagemcom-ores-p1/)
- un câble série USB-A/RJ11 (disponible sur différents sites de vente en ligne (https://bol.com, https://fr.pcm-cable.com, ...);
- un ordinateur (RPi, Lime2, Odroid, un portable récupéré, ... ) avec un connecteur USB-A sous un dérivé de Debian;
- un serveur Web (Apache2, Nginx, ...) supportant au minimum PHP7. Pour exemple, Apache2 sera utilisé dans la suite du document. La commande ci-dessous installera l'ensemble des paquets nécessaires pour Apache2

	`[sudo] apt install apache2 php libapache2-mod-php;`

- Installation de wget et unzip :

	 `[sudo] apt install wget unzip;`


# Installation

Soit le dossier d'installation $WEBSICONIAD (par exemple /srv/httpd/webSiconia).

- Installer setserial

	[`sudo] apt install setserial;`

- Configurer le réseau pour affecter une adresse IP fixe (nommé $IP ci-aprés, comme exemple 192.168.100.11) à l'interface réseau de l'ordinateur et un nom DNS réseau (nommé $NOM ci-aprés,comme exemple siconia.localnetwork), les opérations sont à effectuer suivant votre routeur.

- Créer et e positionner dans le répertoire d'installation $WEBSICONIAD :

	`mkdir -p /srv/httpd/webSiconia;`

	`cd /srv/httpd/webSiconia;`

- Copier le contenu de l'archive dans $WEBSICONIAD :

	`wget -O webSiconia.zip https://framagit.org/BenPig/websiconia/-/archive/main/websiconia-main.zip;`
	
	`unzip webSiconia.zip -d /tmp/;`

	`mv /tmp/websiconia-main/* ./;`

- Suppression de l'archive téléchargée
	rm ./webSiconia.zip;
	
- Éditez le fichier ./siconia.cfg avec votre éditeur (nano, geidt, ...), en précisant le nom d'utilisateur qui sera utilisé par le serveur Web (nommé $WEB_USER, www-data pour Apache2) et le fichier du port série (nommé $PORT, par défaut /dev/ttyUSB0) dans le fichier ./siconia.cfg. La vitesse du port (SPEED) ne devrait pas être modifiée.
	
- Préciser le nom d'utilisateur qui sera utilisé par le serveur Web (nommé $WEB_USER, www-data pour Apache2) et le fichier du port série (nommé $PORT, par défaut /dev/ttyUSB0) dans le fichier ./siconia.cfg. La vitesse du port ($SPEED) ne devrait pas être modifiée.

- Donner le droit d'éxécution à ./siconia.sh

	`[sudo] chmod u+x ./siconia.sh;`

- Lancer le script de configuration et de test

	`[sudo] ./siconia.sh --configure;`

- Configurer le serveur Web. La racine est $WEBSICONIAD/www-data/.
	Pour Apache2,

	`[sudo] cp ./websiconia.conf /etc/apache2/sites-available/;`

		Editer /etc/apache2/sites-available/websiconia.conf avec l'éditeur de votre choix. Remplacer :
			- $WEBSICONIAD par le répertoire d'installation
			- $IP par l'adresse IP
			- $NOM par le nom d'hôte choisi.

	Activation du serveur Web

	`[sudo] a2ensite websiconia.conf;`

	`[sudo] service apache2 restart;`

# Utilisation

`http://$NOM/` ou `http://$NOM/?format=HTML` renvoie une page HTML contenant le tableau de données du Siconia.

`http://$NOM/?format=JSON` renvoie un fichier JSON contenant la structure de données du Siconia.

# Pour tous problèmes, suggestions, ....

Introduisez un ticket sur Framagit ou envoyer un mail à websiconia@phyllum.be.

# Quelques mots sur le fonctionnement pour les développeurs

Le fichier ./www-data/index.php décode les options de paramètres (format et log) et choisit les fichiers modéles à utiliser suivant le format.

Le fichier ./siconia.cfg centralise la configuration "utilisateur".

Le répertoire ./templates/ contient les modéles de fichier des différents formats.

Le fichier ./siconia.php contient 2 classes :

-  Sys donnent les fonctions de base du système : gestion des logs et des erreurs, lecture/écriture de fichier

- Siconia contient la liste des codes de données du Siconia, assure leur extraction, leur vérification (unité). Chaque donnée est identifiée par un nom normalisé.

Le script ./siconia.sh :
- avec l'option --configure (sous root):

		- crée les fichiers temporaires (log et capture de flux) ou les réinitialise;

		- octroye les droits d'accés nécessaires à l'ensemble des fichiers (les droits se font sur le groupe du fichier);

		- effectue un test de capture de flux;
- avec l'option --capture (sous $WEB_USER):

		- capture pendant 1 seconde le flux de données sur le port série du Siconia et le transfére dans le fichier ./siconia.tty.

